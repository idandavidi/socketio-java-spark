# Netty-SocketIO With Java Spark

This repo is an example of how to use [netty-socketio](https://github.com/mrniko/netty-socketio) and [spark-java](http://sparkjava.com).

This is a chat app example based on [netty-socketio-demo](https://github.com/mrniko/netty-socketio-demo).

`spark-java` will be used to access static files (in this case `index.html`).

Run the application and then enter (http://localhost:9091). Watch the application logs at the backend and the changes in the client side. You may want to open more than one tab of chat.